import os
import sys
from urllib.parse import urlparse

import pandas as pd
import requests

CNCF_ITEMS_CSV = "data/items.csv"
GH_TOKEN = os.environ["GH_TOKEN"]


def enrich_project_data_with_languages(row):
    """
    Enrich CNCF Landscape data with primary project language data from GitHub.
    """
    github_repo_url = row.get("github_repo")
    if not github_repo_url:
        raise ValueError("No 'github_repo' URL found in the row.")

    path = urlparse(github_repo_url).path
    parts = path.lstrip("/").split("/")
    if len(parts) < 2:
        raise ValueError("GitHub repository URL is malformed.")

    owner, repo = parts[0], parts[1]

    api_url = f"https://api.github.com/repos/{owner}/{repo}/languages"
    headers = {"Authorization": f"token {GH_TOKEN}"}

    try:
        response = requests.get(url=api_url, headers=headers)
        response.raise_for_status()

        data = response.json()
        primary_language = max(data, key=data.get) if data else None
        row["primary_language"] = primary_language
        print(f'{row["name"]} -> {row["primary_language"]}')

    except requests.RequestException as ex:
        # Handle any requests-related errors here
        print(f"Request to GitHub API failed: {ex}")
        sys.exit()

    return row


df = pd.read_csv(CNCF_ITEMS_CSV)

# Select active CNCF projects
filtered_df = df[
    df["relation"].isin(["sandbox", "incubating", "graduated"]) & df["github_repo"].notnull()
]

# Enrich
enriched_df = filtered_df.apply(enrich_project_data_with_languages, axis=1)

# Drop projects w/o language
enriched_df.dropna(subset=['primary_language'], inplace=True)

# Output JSON
enriched_df[["name", "github_repo", "relation", "primary_language"]].to_json(
    "src/data.json", orient="records"
)
