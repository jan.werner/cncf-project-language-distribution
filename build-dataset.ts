import { Octokit } from "@octokit/rest";
import * as Papa from "papaparse";
import fs from 'node:fs/promises'
import { z } from "zod";

const cncf_items_csv = "data/items.csv";
const octokit = new Octokit({ auth: process.env.GH_TOKEN });

const GitHubRepoSchema = z.string().url().transform((url) => {
  const regex = /^https:\/\/github\.com\/([^\/]+)\/([^\/]+)$/;
  const match = regex.exec(url)
  if (!match) {
    throw new Error("GitHub repository URL is malformed.");
  }

  const [, owner, repo] = match;
  return { owner, repo };
});

const ProjectRowSchema = z.object({
  name: z.string(),
  github_repo: GitHubRepoSchema,
  relation: z.enum(['sandbox', 'incubating', 'graduated']),
  primary_language: z.string().optional(),
});

type ProjectRow = z.infer<typeof ProjectRowSchema>;

interface LanguageData {
  [key: string]: number;
}

async function enrichProjectDataWithLanguages (row: ProjectRow): Promise<ProjectRow> {
  const { owner, repo } = row.github_repo

  try {
    const response = await octokit.repos.listLanguages({
      owner,
      repo
    })

    const languages: LanguageData = response.data

    const languageEntries = Object.entries(languages)

    const maxLanguageEntry = languageEntries.reduce((maxEntry, currentEntry) => {
      const [currentLanguage, currentLines] = currentEntry
      const maxLines = maxEntry.lines || 0;

      return currentLines > maxLines ? { lang: currentLanguage, lines: currentLines } : maxEntry;
    }, { lang: '', lines: 0 });

    const primaryLanguage = maxLanguageEntry.lang;

    console.info(`${row.name} -> ${primaryLanguage}`)

    return {
      ...row,
      primary_language: primaryLanguage,
    };
  } catch (error: unknown) {
    if (error instanceof Error) {
      throw new Error(`Request to GitHub API failed for ${row.name}: ${error.message}`)
    }
    throw new Error(`Request to GitHub API failed for ${row.name} and an unknown error occurred.`)
  }
}

async function processCSV() {
  const parseResult = Papa.parse(cncf_items_csv, {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
  });

  // Pre-filter and validate the data
  const validFilteredRows: ProjectRow[] = parseResult.data
    .filter((row: any) =>
      row.github_repo && ['sandbox', 'incubating', 'graduated'].includes(row.relation)
    )
    .map((row: any) => {
      try {
        return ProjectRowSchema.parse(row);
      } catch (error) {
        console.error(error);
        return null;
      }
    })
    .filter((row): row is ProjectRow => row !== null);

  try {
    const enrichedDataPromises = validFilteredRows.map(enrichProjectDataWithLanguages);
    const enrichedDataResults = await Promise.all(enrichedDataPromises);
    const enrichedData = enrichedDataResults.filter(row => row !== null);

    await fs.writeFile('src/data.json', JSON.stringify(enrichedData, null, 2));
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

processCSV().catch(console.error);
