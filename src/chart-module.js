import { Chart, DoughnutController, ArcElement, Tooltip, Legend } from 'chart.js';
import 'groupby-polyfill/lib/polyfill.js'

import data from './data.json';
import colors from './colors.json';

Chart.register(DoughnutController, ArcElement, Tooltip, Legend);

function processLanguageData(data, relation, colors) {
  const filteredData = data.filter(item => item.relation === relation);
  const groupedByLanguage = Object.groupBy(filteredData, item => item.primary_language);

  const languageDistribution = {};
  for (const [language, items] of Object.entries(groupedByLanguage)) {
    languageDistribution[language] = items.length;
  }

  return {
    labels: Object.keys(languageDistribution),
    datasets: [{
      label: `Language Distribution (${relation})`,
      data: Object.values(languageDistribution),
      backgroundColor: Object.keys(languageDistribution).map(language =>
        colors[language] ? colors[language].color : '#999'
      ),
    }]
  };
}

function createChart(ctx, chartData) {
  var styleElement = document.querySelector('.chart');
  var chartFontFamily = window.getComputedStyle(styleElement).fontFamily;

  return new Chart(ctx, {
    type: 'doughnut',
    data: chartData,
    options: {
      responsive: true,
      plugins: {
        legend: {
          position: 'bottom',
          padding: 20,
          labels: {
            font: {
              family: chartFontFamily,
            },
            padding: 20,
          },
        },
        tooltip: {
          bodyFont: {
            family: chartFontFamily,
          }
        }
      }
    },
  });
}

async function drawCharts() {
  try {
    const relations = ['incubating', 'sandbox', 'graduated'];

    relations.forEach(relation => {
      const chartData = processLanguageData(data, relation, colors);
      const ctx = document.getElementById(`languageDistributionChart-${relation}`).getContext('2d');
      createChart(ctx, chartData);
    });
  } catch (error) {
    console.error('Error loading JSON data: ', error);
  }
}

drawCharts();
